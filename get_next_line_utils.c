/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_utils.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/27 22:41:22 by pmarquis          #+#    #+#             */
/*   Updated: 2022/10/27 23:53:12 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

char	*_reader_reset(t_reader *m)
{
	m->fd = 0;
	if (m->str)
		free(m->str);
	m->str = 0;
	m->slen = 0;
	return (0);
}

void	*ft_memchr(const void *s, int c, size_t n)
{
	while (n--)
	{
		if (*((unsigned char *)s) == (unsigned char)c)
			return ((void *)s);
		(void)(unsigned char *)++s;
	}
	return (0);
}

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	void	*ret;

	if (dst == src || n == 0)
		return (dst);
	ret = dst;
	while (n--)
		*((unsigned char *)dst++) = *((unsigned char *)src++);
	return (ret);
}
