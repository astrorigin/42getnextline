/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/27 11:21:57 by pmarquis          #+#    #+#             */
/*   Updated: 2022/10/27 22:40:20 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# include <stdlib.h>
# include <unistd.h>

# ifndef BUFFER_SIZE
#  define BUFFER_SIZE	42
# endif

# ifndef SEPARATOR_CHAR
#  define SEPARATOR_CHAR	'\n'
# endif

typedef struct s_reader
{
	int		fd;
	char	*str;
	size_t	slen;
}	t_reader;

char	*get_next_line(int fd);

/*
 *	Utils
 */
char	*_reader_reset(t_reader *m);
void	*ft_memchr(const void *s, int c, size_t n);
void	*ft_memcpy(void *dst, const void *src, size_t n);

#endif
