/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_bonus.h                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/27 11:21:57 by pmarquis          #+#    #+#             */
/*   Updated: 2022/10/28 11:05:41 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_BONUS_H
# define GET_NEXT_LINE_BONUS_H

# include <stdlib.h>
# include <unistd.h>

# ifndef BUFFER_SIZE
#  define BUFFER_SIZE	42
# endif

# ifndef SEPARATOR_CHAR
#  define SEPARATOR_CHAR	'\n'
# endif

typedef struct s_reader
{
	int				fd;
	char			*str;
	size_t			slen;
	struct s_reader	*next;
	struct s_reader	**first;
}	t_reader;

char		*get_next_line(int fd);

/*
 *	Utils
 */
char		*_reader_free(t_reader **readers);
t_reader	*_reader_get(t_reader **readers, int fd);
char		*_reader_reset(t_reader *m);
void		*ft_memchr(const void *s, int c, size_t n);
void		*ft_memcpy(void *dst, const void *src, size_t n);

#endif
