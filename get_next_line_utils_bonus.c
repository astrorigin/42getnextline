/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_utils_bonus.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/27 22:41:22 by pmarquis          #+#    #+#             */
/*   Updated: 2022/10/28 12:01:36 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line_bonus.h"

char	*_reader_free(t_reader **readers)
{
	t_reader	*m;
	t_reader	*next;

	m = *readers;
	while (m)
	{
		next = m->next;
		if (m->str)
			free(m->str);
		free(m);
		m = next;
	}
	*readers = 0;
	return (0);
}

t_reader	*_reader_get(t_reader **readers, int fd)
{
	t_reader	*m;
	t_reader	*prev;

	m = *readers;
	prev = 0;
	while (m)
	{
		if (m->fd == fd)
			return (m);
		prev = m;
		m = m->next;
	}
	m = malloc(sizeof(t_reader));
	if (!m)
		return (0);
	m->fd = fd;
	m->str = 0;
	m->slen = 0;
	m->next = 0;
	m->first = readers;
	if (prev)
		prev->next = m;
	else
		*readers = m;
	return (m);
}

char	*_reader_reset(t_reader *m)
{
	t_reader	*r;
	t_reader	*prev;

	r = *(m->first);
	prev = 0;
	while (r)
	{
		if (r == m)
		{
			if (prev)
				prev->next = m->next;
			else
				*(m->first) = m->next;
			if (m->str)
				free(m->str);
			free(m);
			return (0);
		}
		prev = r;
		r = r->next;
	}
	return (0);
}

void	*ft_memchr(const void *s, int c, size_t n)
{
	while (n--)
	{
		if (*((unsigned char *)s) == (unsigned char)c)
			return ((void *)s);
		(void)(unsigned char *)++s;
	}
	return (0);
}

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	void	*ret;

	if (dst == src || n == 0)
		return (dst);
	ret = dst;
	while (n--)
		*((unsigned char *)dst++) = *((unsigned char *)src++);
	return (ret);
}
