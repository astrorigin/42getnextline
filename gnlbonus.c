/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gnlbonus.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/28 00:10:02 by pmarquis          #+#    #+#             */
/*   Updated: 2022/10/28 00:10:06 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line_bonus.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

static void	_putstr(char *s)
{
	while (*s)
		write(1, s++, 1);
}

static int	dostdin(void)
{
	char	*line;

	line = (char *) 1;
	while (line)
	{
		line = get_next_line(0);
		if (line)
		{
			_putstr(line);
			free(line);
		}
	}
	return (0);
}

static int	filesloop(int *fd, int num)
{
	int		i;
	char	*line;

	i = 0;
	line = (char *) 1;
	while (line)
	{
		line = get_next_line(fd[i]);
		if (line)
		{
			_putstr(line);
			free(line);
		}
		if (i == num - 1)
			i = 0;
		else
			++i;
	}
	i = 0;
	while (i < num)
		close(fd[i++]);
	free(fd);
	return (0);
}

static int	dofiles(int num, char *names[])
{
	int		*fd;
	int		i;

	fd = malloc(sizeof(int) * num);
	if (!fd)
		return (1);
	i = 0;
	while (i < num)
	{
		fd[i] = open(names[i], O_RDONLY);
		if (fd[i++] < 0)
			return (1);
	}
	return (filesloop(fd, num));
}

int	main(int argc, char *argv[])
{
	if (argc == 1 || argv[1][0] == '-')
		return (dostdin());
	return (dofiles(argc - 1, &argv[1]));
}
