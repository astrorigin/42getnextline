# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2022/10/27 12:20:26 by pmarquis          #+#    #+#              #
#    Updated: 2022/10/28 00:06:13 by pmarquis         ###   lausanne.ch        #
#                                                                              #
# **************************************************************************** #

.PHONY: all clean fclean mrproper re bonus

NAME = gnl
NAME2 = gnlbonus

CC := gcc
CFLAGS := -Wall -Wextra -Werror #-fsanitize=address -g

SRC = gnl.c get_next_line.c get_next_line_utils.c
OBJ = $(patsubst %.c,%.o,$(SRC))
INC = get_next_line.h

SRC2 = gnlbonus.c get_next_line_bonus.c get_next_line_utils_bonus.c
OBJ2 = $(patsubst %.c,%.o,$(SRC2))
INC2 = get_next_line_bonus.h

.c.o:
	$(CC) -c $(CFLAGS) -o $(<:.c=.o) $<

.DEFAULT_GOAL = all

all: $(NAME)

$(NAME): $(OBJ)
	$(CC) $(CFLAGS) $^ -o $@

$(NAME2): $(OBJ2)
	$(CC) $(CFLAGS) $^ -o $@

clean:
	rm -f $(OBJ) $(OBJ2)

fclean: clean
	rm -f $(NAME) $(NAME2)

mrproper: fclean

re: fclean all

bonus: $(NAME2)

###
get_next_line.o: $(INC)
get_next_line_bonus.o: $(INC2)
get_next_line_utils.o: $(INC)
get_next_line_utils_bonus.o: $(INC2)
gnl.o: $(INC)
gnlbonus.o: $(INC2)

