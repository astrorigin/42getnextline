/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gnl.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/27 12:22:20 by pmarquis          #+#    #+#             */
/*   Updated: 2022/10/27 18:47:25 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

static void	_putstr(char *s)
{
	while (*s)
		write(1, s++, 1);
}

int	main(int argc, char *argv[])
{
	int		fd;
	char	*line;

	if (argc == 1 || argv[1][0] == '-')
		fd = 0;
	else
		fd = open(argv[1], O_RDONLY);
	if (fd < 0)
		return (1);
	line = (char *) 1;
	while (line)
	{
		line = get_next_line(fd);
		if (line)
		{
			_putstr(line);
			free(line);
		}
	}
	return (0);
}
